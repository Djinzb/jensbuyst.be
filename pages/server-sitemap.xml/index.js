import { getServerSideSitemapLegacy } from "next-sitemap";
import { sanityClient } from "../../lib/sanity.server";
import { postSlugsQuery } from "../../lib/queries";

export const getServerSideProps = async (ctx) => {
  const paths = await sanityClient.fetch(postSlugsQuery);

  const fields = paths.map((slug) => ({
    loc: `https://www.jensbuyst.be/brain-dumps/${slug}`,
    lastmod: new Date().toISOString(),
  }));

  return getServerSideSitemapLegacy(ctx, fields);
};

export default function ServerSiteMap() {}
