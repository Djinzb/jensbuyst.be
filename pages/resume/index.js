import React from "react";
import Head from "next/head";
import Resume from "../../components/resume/Resume";
import resumeData from "../../data/resume.json";
import styles from "../../styles/Resume.module.scss";

const Home = () => {
  const handleDownload = () => {
    const link = document.createElement('a');
    link.href = '/resume.pdf';
    link.download = 'Jens_Buyst_Resume.pdf';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return (
    <div className={styles.container}>
      <Head>
        <title>{resumeData.name} - Resume</title>
        <meta name="description" content={`${resumeData.name}'s resume`} />
        <link rel="icon" href="/favicon.ico" />
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600;700&display=swap" rel="stylesheet" />
      </Head>

      <main>
        <button className={styles.downloadButton} onClick={handleDownload}>
          Download PDF
        </button>
        <Resume data={resumeData} />
      </main>
    </div>
  );
};

export default Home;

