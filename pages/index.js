import Head from "next/head";
import { Navigation } from "../components/nav";
import Layout from "../components/general/layout";
import Image from "next/image";
import styles from "../styles/index.module.scss";
import cycling from "../public/cycling.jpg";
import { getClient, overlayDrafts } from "../lib/sanity.server";
import { homePageQuery } from "../lib/queries";

const Home = ({ content }) => {
  const { intro } = content?.[0];

  return (
    <Layout>
      <Head>
        <title>Jens Buyst | Software developer</title>
      </Head>
      <section className={styles.grid}>
        <Navigation title={"Jens Buyst"} />
        <article>{intro}</article>
        <div className={styles.images}>
          <Image
            src={cycling}
            sizes="100vw"
            priority={true}
            placeholder={"blur"}
            alt={"Image of Jens riding a bike"}
          />
        </div>
      </section>
    </Layout>
  );
};

export async function getStaticProps() {
  const content = overlayDrafts(await getClient().fetch(homePageQuery));
  return {
    props: { content },
  };
}

export default Home;
