import "../styles/index.css";
import AnalyticsWrapper from "../components/general/analytics";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Component {...pageProps} />
      <AnalyticsWrapper />
    </>
  );
}

export default MyApp;
