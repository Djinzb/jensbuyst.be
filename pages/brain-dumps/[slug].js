import { postQuery, postSlugsQuery } from "../../lib/queries";
import { getClient, overlayDrafts, sanityClient } from "../../lib/sanity.server";
import Post from "../../components/blog/post";

export default function PostPage({ data }) {
  return <Post data={data} />;
}

export async function getStaticProps({ params }) {
  const { post, morePosts } = await getClient().fetch(postQuery, {
    slug: params.slug,
  });

  return {
    props: {
      data: {
        post,
        morePosts: overlayDrafts(morePosts),
      },
    },
    revalidate: process.env.SANITY_REVALIDATE_SECRET ? undefined : 60,
  };
}

export async function getStaticPaths() {
  const paths = await sanityClient.fetch(postSlugsQuery);
  return {
    paths: paths.map((slug) => ({ params: { slug } })),
    fallback: true,
  };
}
