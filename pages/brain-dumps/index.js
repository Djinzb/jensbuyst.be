import Head from "next/head";
import { indexQuery } from "../../lib/queries";
import { getClient, overlayDrafts } from "../../lib/sanity.server";
import Layout from "../../components/general/layout";
import { Navigation } from "../../components/nav";
import HeroPost from "../../components/blog/hero-post";
import MoreStories from "../../components/blog/more-stories";
import styles from "../../components/blog/hero-post/heroPost.module.scss";

export default function IndexPage({ allPosts }) {
  const [heroPost, ...morePosts] = allPosts || [];
  const { title, coverImage, date, slug, excerpt, estimatedReadingTime, viewCount } =
    heroPost;

  return (
    <>
      <Layout>
        <Head>
          <title>Jens Buyst | Brain dumps</title>
        </Head>
        <Navigation title={"Brain dumps"} />
        <section className={styles.blogLayout}>
          {heroPost && (
            <HeroPost
              title={title}
              coverImage={coverImage}
              date={date}
              slug={slug}
              excerpt={excerpt}
              timeToRead={estimatedReadingTime}
              viewCount={viewCount}
            />
          )}
          {morePosts.length > 0 && <MoreStories posts={morePosts} />}
        </section>
      </Layout>
    </>
  );
}

export async function getStaticProps() {
  const allPosts = overlayDrafts(await getClient().fetch(indexQuery));
  return {
    props: { allPosts },
    revalidate: process.env.SANITY_REVALIDATE_SECRET ? undefined : 60
  };
}
