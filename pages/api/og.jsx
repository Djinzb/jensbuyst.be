import { ImageResponse } from "@vercel/og";

export const config = {
  runtime: "edge"
};

const font = fetch(new URL('../../public/fonts/kanit.ttf', import.meta.url)).then(
  (res) => res.arrayBuffer(),
);

export default async function handler(req) {
  const fontData = await font;
  try {
    const { searchParams } = new URL(req.url);

    const hasTitle = searchParams.has("title");
    const hasImageUrl = searchParams.has("imageUrl");

    const title = hasTitle
      ? searchParams.get("title")?.slice(0, 100)
      : "Jens Buyst";


    const image = hasImageUrl ? <img src={searchParams.get("imageUrl")} width={1000} height={425} alt={`Image for blog post titled ${searchParams.get("title")?.slice(0, 100)}`} style={{zIndex: '-1'}}/> : '';

    return (hasTitle && hasImageUrl) ? new ImageResponse(
      (
        <div
          style={{
            backgroundColor: "white",
            backgroundSize: "150px 150px",
            height: "100%",
            width: "100%",
            display: "flex",
            textAlign: "center",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
            flexWrap: "nowrap",
            fontFamily: '"Kanit"',
            position: 'relative'
          }}
        >
          <div style={{display: 'flex', border: '15px solid black', position: 'relative', zIndex: "-1"}}>
            {image}
            <img alt="Logo Jens Buyst"
                 height={100}
                 width={100}
                 src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDAiIGhlaWdodD0iNDAiIHZpZXdCb3g9IjAgMCA0MCA0MCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHJlY3QgeD0iMS41IiB5PSIxLjUiIHdpZHRoPSIzNyIgaGVpZ2h0PSIzNyIgZmlsbD0id2hpdGUiIHN0cm9rZT0iYmxhY2siIHN0cm9rZS13aWR0aD0iMyIvPgo8cGF0aCBkPSJNOS45NSAyOS4yNUM5LjI1IDI5LjI1IDguNTgzMzMgMjkuMiA3Ljk1IDI5LjFDNy4zMTY2NyAyOS4wMTY3IDYuNzA4MzMgMjguODc1IDYuMTI1IDI4LjY3NVYyNC43NzVDNi41MjUgMjQuOTQxNyA2Ljk3NSAyNS4wNjY3IDcuNDc1IDI1LjE1QzcuOTc1IDI1LjIzMzMgOC40NzUgMjUuMjc1IDguOTc1IDI1LjI3NUM5Ljg3NSAyNS4yNzUgMTAuNTE2NyAyNS4wOTE3IDEwLjkgMjQuNzI1QzExLjI4MzMgMjQuMzU4MyAxMS40NzUgMjMuNzUgMTEuNDc1IDIyLjlWMTIuOUgxNi4xNzVWMjMuNjVDMTYuMTc1IDI1LjQgMTUuNjQxNyAyNi43NzUgMTQuNTc1IDI3Ljc3NUMxMy41MjUgMjguNzU4MyAxMS45ODMzIDI5LjI1IDkuOTUgMjkuMjVaTTE4LjMwNjYgMjlWMTIuOUgyNy4xNTY2QzI4Ljc5IDEyLjkgMzAuMDMxNiAxMy4yODMzIDMwLjg4MTYgMTQuMDVDMzEuNzMxNiAxNC44MTY3IDMyLjE1NjYgMTUuOTE2NyAzMi4xNTY2IDE3LjM1QzMyLjE1NjYgMTguMTgzMyAzMS45OTgzIDE4Ljg4MzMgMzEuNjgxNiAxOS40NUMzMS4zODE2IDIwLjAxNjcgMzAuOTgxNiAyMC40NjY3IDMwLjQ4MTYgMjAuOEMzMS4xNjUgMjEuMDY2NyAzMS43MDY2IDIxLjQ1ODMgMzIuMTA2NiAyMS45NzVDMzIuNTIzMyAyMi40NzUgMzIuNzMxNiAyMy4yMjUgMzIuNzMxNiAyNC4yMjVDMzIuNzMxNiAyNS43NDE3IDMyLjI2NSAyNi45MTY3IDMxLjMzMTYgMjcuNzVDMzAuMzk4MyAyOC41ODMzIDI5LjA3MzMgMjkgMjcuMzU2NiAyOUgxOC4zMDY2Wk0yMi44MzE2IDE5LjMyNUgyNS44ODE2QzI3LjAzMTYgMTkuMzI1IDI3LjYwNjYgMTguODA4MyAyNy42MDY2IDE3Ljc3NUMyNy42MDY2IDE3LjI1ODMgMjcuNDY1IDE2Ljg2NjcgMjcuMTgxNiAxNi42QzI2LjkxNSAxNi4zMzMzIDI2LjQ0IDE2LjIgMjUuNzU2NiAxNi4ySDIyLjgzMTZWMTkuMzI1Wk0yMi44MzE2IDI1LjcyNUgyNi4wODE2QzI2LjcxNSAyNS43MjUgMjcuMTgxNiAyNS41OTE3IDI3LjQ4MTYgMjUuMzI1QzI3Ljc4MTYgMjUuMDU4MyAyNy45MzE2IDI0LjYxNjcgMjcuOTMxNiAyNEMyNy45MzE2IDIzLjQ4MzMgMjcuNzgxNiAyMy4wOTE3IDI3LjQ4MTYgMjIuODI1QzI3LjE4MTYgMjIuNTU4MyAyNi42ODE2IDIyLjQyNSAyNS45ODE2IDIyLjQyNUgyMi44MzE2VjI1LjcyNVoiIGZpbGw9ImJsYWNrIi8+Cjwvc3ZnPgo="
                 style={{position: 'absolute', top: '10px', right: '10px'}}
            />
          </div>
          <div
            style={{
              fontSize: 60,
              fontStyle: "normal",
              letterSpacing: "-0.025em",
              color: "black",
              lineHeight: 1.4,
              whiteSpace: "pre-wrap"
            }}
          >
            {title}
          </div>
        </div>
      ),
      {
        width: 1200,
        height: 630,
        fonts: [
          {
            name: 'Kanit',
            data: fontData,
            style: 'normal',
          },
        ],
      }
    ) : new ImageResponse(
      (
        <div
          style={{
            backgroundColor: "white",
            backgroundSize: "150px 150px",
            height: "100%",
            width: "100%",
            display: "flex",
            textAlign: "center",
            alignItems: "center",
            justifyContent: "center",
            flexDirection: "column",
            flexWrap: "nowrap",
            fontFamily: '"Kanit"',
          }}
        >
          <div
            style={{
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              justifyItems: "center"
            }}
          >
            <img alt="Logo Jens Buyst"
                 height={200}
                 width={200}
                 style={{ margin: "0 30px" }}
                 src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDAiIGhlaWdodD0iNDAiIHZpZXdCb3g9IjAgMCA0MCA0MCIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPHJlY3QgeD0iMS41IiB5PSIxLjUiIHdpZHRoPSIzNyIgaGVpZ2h0PSIzNyIgZmlsbD0id2hpdGUiIHN0cm9rZT0iYmxhY2siIHN0cm9rZS13aWR0aD0iMyIvPgo8cGF0aCBkPSJNOS45NSAyOS4yNUM5LjI1IDI5LjI1IDguNTgzMzMgMjkuMiA3Ljk1IDI5LjFDNy4zMTY2NyAyOS4wMTY3IDYuNzA4MzMgMjguODc1IDYuMTI1IDI4LjY3NVYyNC43NzVDNi41MjUgMjQuOTQxNyA2Ljk3NSAyNS4wNjY3IDcuNDc1IDI1LjE1QzcuOTc1IDI1LjIzMzMgOC40NzUgMjUuMjc1IDguOTc1IDI1LjI3NUM5Ljg3NSAyNS4yNzUgMTAuNTE2NyAyNS4wOTE3IDEwLjkgMjQuNzI1QzExLjI4MzMgMjQuMzU4MyAxMS40NzUgMjMuNzUgMTEuNDc1IDIyLjlWMTIuOUgxNi4xNzVWMjMuNjVDMTYuMTc1IDI1LjQgMTUuNjQxNyAyNi43NzUgMTQuNTc1IDI3Ljc3NUMxMy41MjUgMjguNzU4MyAxMS45ODMzIDI5LjI1IDkuOTUgMjkuMjVaTTE4LjMwNjYgMjlWMTIuOUgyNy4xNTY2QzI4Ljc5IDEyLjkgMzAuMDMxNiAxMy4yODMzIDMwLjg4MTYgMTQuMDVDMzEuNzMxNiAxNC44MTY3IDMyLjE1NjYgMTUuOTE2NyAzMi4xNTY2IDE3LjM1QzMyLjE1NjYgMTguMTgzMyAzMS45OTgzIDE4Ljg4MzMgMzEuNjgxNiAxOS40NUMzMS4zODE2IDIwLjAxNjcgMzAuOTgxNiAyMC40NjY3IDMwLjQ4MTYgMjAuOEMzMS4xNjUgMjEuMDY2NyAzMS43MDY2IDIxLjQ1ODMgMzIuMTA2NiAyMS45NzVDMzIuNTIzMyAyMi40NzUgMzIuNzMxNiAyMy4yMjUgMzIuNzMxNiAyNC4yMjVDMzIuNzMxNiAyNS43NDE3IDMyLjI2NSAyNi45MTY3IDMxLjMzMTYgMjcuNzVDMzAuMzk4MyAyOC41ODMzIDI5LjA3MzMgMjkgMjcuMzU2NiAyOUgxOC4zMDY2Wk0yMi44MzE2IDE5LjMyNUgyNS44ODE2QzI3LjAzMTYgMTkuMzI1IDI3LjYwNjYgMTguODA4MyAyNy42MDY2IDE3Ljc3NUMyNy42MDY2IDE3LjI1ODMgMjcuNDY1IDE2Ljg2NjcgMjcuMTgxNiAxNi42QzI2LjkxNSAxNi4zMzMzIDI2LjQ0IDE2LjIgMjUuNzU2NiAxNi4ySDIyLjgzMTZWMTkuMzI1Wk0yMi44MzE2IDI1LjcyNUgyNi4wODE2QzI2LjcxNSAyNS43MjUgMjcuMTgxNiAyNS41OTE3IDI3LjQ4MTYgMjUuMzI1QzI3Ljc4MTYgMjUuMDU4MyAyNy45MzE2IDI0LjYxNjcgMjcuOTMxNiAyNEMyNy45MzE2IDIzLjQ4MzMgMjcuNzgxNiAyMy4wOTE3IDI3LjQ4MTYgMjIuODI1QzI3LjE4MTYgMjIuNTU4MyAyNi42ODE2IDIyLjQyNSAyNS45ODE2IDIyLjQyNUgyMi44MzE2VjI1LjcyNVoiIGZpbGw9ImJsYWNrIi8+Cjwvc3ZnPgo="
            />
          </div>
          <div
            style={{
              fontSize: 60,
              fontStyle: "normal",
              letterSpacing: "-0.025em",
              color: "black",
              marginTop: 30,
              padding: "0 120px",
              lineHeight: 1.4,
              whiteSpace: "pre-wrap"
            }}
          >
            {title}
          </div>
        </div>
      ),
      {
        width: 1200,
        height: 630,
        fonts: [
          {
            name: 'Kanit',
            data: fontData,
            style: 'normal',
          },
        ],
      }
    );
  } catch (e) {
    console.error(`${e.message}`);
    return new Response(`Failed to generate the image`, {
      status: 500
    });
  }
}
