import sanityClient from "@sanity/client";
import { NextApiRequest, NextApiResponse } from "next";

const config = {
  dataset: process.env.SANITY_STUDIO_API_DATASET,
  projectId: process.env.SANITY_STUDIO_API_PROJECT_ID,
  useCdn: process.env.NODE_ENV === 'production',
  token: process.env.SANITY_API_WRITE_TOKEN,
}
const client = sanityClient(config)

export default async function createViewCount(req : NextApiRequest, res: NextApiResponse) {
  const { _id } = req.query;

  try {
    await client.patch(`${_id}`).inc({viewCount: 1}).commit();
  } catch (err) {
    console.error(err)
    return res.status(500).json({ message: `Couldn't submit viewCount`, err })
  }
  return res.status(200).json({ message: 'ViewCount submitted' })
}
