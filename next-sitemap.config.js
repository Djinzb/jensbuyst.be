module.exports = {
  siteUrl: 'https://www.jensbuyst.be',
  generateRobotsTxt: true,
  exclude: ['/server-sitemap.xml'], // <= exclude here
  robotsTxtOptions: {
    additionalSitemaps: [
      'https://www.jensbuyst.be/server-sitemap.xml', // <==== Add here
    ],
  },
}
