import Meta from "../general/meta";
import { Kanit } from "@next/font/google";

const font = Kanit({
  subsets: ["latin"],
  weight: ["100", "200", "300", "400", "500"],
});

export default function Layout({ children }) {
  const styles = {minHeight: "100vh", maxWidth: "1600px", margin: "0 auto"}
  return (
    <>
      <Meta />
      <main className={font.className} style={styles}>
        {children}
      </main>
    </>
  );
}
