import React from "react";
import Image from "next/image";
import styles from "../../styles/Resume.module.scss";

const Resume = ({ data }) => {
  return (
    <div className={styles.resume}>
      <header className={styles.header}>
        <div className={styles.headerContent}>
          <h1>{data.name}</h1>
          <h2>{data.title}</h2>
        </div>
      </header>

      <section className={styles.about}>
        <h3>ABOUT ME</h3>
        <p>{data.about}</p>
      </section>

      <section className={styles.experience}>
        <h3>EXPERIENCE</h3>
        {data.experience.map((exp, index) => (
          <div key={index} className={styles.experienceItem}>
            <span className={styles.date}>{exp.startDate} - {exp.endDate}</span>
            <span className={styles.title}>{exp.title}</span>
            <span className={styles.company}>{exp.company}</span>
          </div>
        ))}
      </section>

      <section className={styles.skills}>
        <h3>RELEVANT SKILLS</h3>
        <ul>
          {data.skills.map((skill, index) => (
            <li key={index}>{skill}</li>
          ))}
        </ul>
      </section>

      <section className={styles.skills}>
        <h3>DEVELOPMENT INTERESTS</h3>
        <ul>
          {data.interests.map((interest, index) => (
            <li key={index}>{interest}</li>
          ))}
        </ul>
      </section>

      <section className={styles.projects}>
        <h3>PROJECTS</h3>
        {data.clients.map((client, clientIndex) => (
          <div key={clientIndex} className={styles.clientSection}>
            <div className={styles.clientHeader}>
              <Image
                src={client.logo}
                alt={`${client.name} logo`}
                width={200}
                height={100}
                className={styles.clientLogo}
                style={{ objectFit: 'contain' }}
              />
              <h4>{client.name}</h4>
            </div>
            {client.projects.map((project, projectIndex) => (
              <div key={projectIndex} className={styles.projectItem}>
                <h5>{project.name}</h5>
                <p><strong>Description:</strong> {project.description}</p>
                <p><strong>Role:</strong> {project.role}</p>
                <p><strong>Technologies:</strong></p>
                <ul>
                  {Object.entries(project.technologies).map(([key, value]) => (
                    <li key={key}><strong>{key.charAt(0).toUpperCase() + key.slice(1)}:</strong> {value}</li>
                  ))}
                </ul>
                <p><strong>Date:</strong> {project.startDate} - {project.endDate}</p>
              </div>
            ))}
          </div>
        ))}
      </section>
    </div>
  );
};

export default Resume;

