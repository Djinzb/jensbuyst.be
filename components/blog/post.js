import { useRouter } from "next/router";
import ErrorPage from "next/error";
import Layout from "../general/layout";
import PostTitle from "./post-title";
import Head from "next/head";
import PostHeader from "./post-header";
import PostBody from "./post-body";
import MoreStories from "./more-stories";
import { urlForImage } from "../../lib/sanity";
import { useEffect } from "react";
import { Comments } from "./comments";
import { RiArrowLeftCircleLine } from "react-icons/ri";
import Link from "next/link";

export default function Post({ data = {} }) {
  const router = useRouter();


  const { post, morePosts } = data;
  const slug = post?.slug;
  const id = post?._id;
  const viewCount = post?.viewCount;

  if (!router.isFallback && !slug) {
    return <ErrorPage statusCode={404} />;
  }

  const imageUrl = post?.coverImage?.asset?._ref ? urlForImage(post.coverImage)
    .width(1200)
    .height(627)
    .fit("crop")
    .url() : "";

  useEffect(() => {
    const registerView = () =>
      fetch(`/api/views/${post?._id}`, {
        method: "POST"
      });
    registerView();
  }, [id]);

  return (
    <Layout>
      <nav className={"m-4"}>
        <ul><Link href={"/brain-dumps"}>
          <li className={"hover:underline text-2xl flex items-center gap-1"}><RiArrowLeftCircleLine /> Back</li>
        </Link></ul>
      </nav>
      {router.isFallback ? (
        <PostTitle>Loading…</PostTitle>
      ) : (
        <>
          <Head>
            <title>Jens Buyst | {post.title}</title>
            <meta property="og:image"
                  content={`https://www.jensbuyst.be/api/og?title=${encodeURIComponent(post.title)}&imageUrl=${imageUrl}`} />
            <meta property="og:title" content={`Brain dumps | ${post.title}`} />
            <meta property="og:description" content={post.excerpt} />
          </Head>
          <article>
            <PostHeader
              title={post.title}
              coverImage={post.coverImage}
              date={post.date}
              viewCount={viewCount}
              timeToRead={post.estimatedReadingTime}
            />
            <div style={{ margin: "0 1rem" }}>
              <PostBody content={post.content} />
            </div>
          </article>
          <Comments comments={post.comments} _id={post._id} />
          {morePosts.length > 0 && <MoreStories posts={morePosts} />}
        </>
      )}
    </Layout>
  );
}
