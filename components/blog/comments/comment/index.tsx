import styles from "./comment.module.scss";
import Date from "../../date";

interface CommentProps  {
  initials: string,
  name: string,
  dateString: string,
  text: string,
  unapproved?: boolean
}

export const Comment = (props: CommentProps) => {
  return <li className={`${styles.comment} ${props.unapproved ? styles.unapproved : ''}`}>
    <div className={styles.authorInfo}>
      <div className={styles.name}>
        <div className={styles.avatar}>{props.initials}</div>
        <h4>{props.name} {props.unapproved && <span className={styles.unapprovedText}>Awaiting approval</span>}</h4>
      </div>
      <Date dateString={props.dateString} />
    </div>
    <p>{props.text}</p>

  </li>;
};
