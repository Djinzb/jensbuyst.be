import CommentForm from "./commentForm";
import styles from "./comments.module.scss";
import { Comment } from "./comment";

export function Comments({ comments = [], _id }) {
  return (
    <>
      <hr style={{marginTop: '3rem'}}/>
      <section className={styles.commentSection}>
        <CommentForm _id={_id} />
        <div className={styles.commentList}>
          <ul>
            {comments?.map(({ _id, _createdAt, name, text }) => (
              <Comment key={_id} initials={getInitials(name)} name={name} dateString={_createdAt} text={text} />
            ))}
          </ul>
        </div>
      </section>
    </>
  );
}

export function getInitials(name): string {
  let rgx = new RegExp(/(\p{L}{1})\p{L}+/, "gu");

  let initials = [...name.matchAll(rgx)] || [];

  initials = (
    (initials.shift()?.[1] || "") + (initials.pop()?.[1] || "")
  ).toUpperCase();

  return initials.toString();
}
