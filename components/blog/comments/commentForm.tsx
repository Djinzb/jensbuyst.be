import { useState } from "react";
import { useForm } from "react-hook-form";
import styles from "./comments.module.scss";
import { Comment } from "./comment";
import { getInitials } from "./index";

interface FormDataProps {
  name: "string",
  text: "string"
}

export default function CommentForm({ _id }) {
  const [formData, setFormData] = useState<FormDataProps | undefined>();
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [hasSubmitted, setHasSubmitted] = useState(false);

  const { register, handleSubmit } = useForm();

  const onSubmit = async data => {
    setIsSubmitting(true);
    setFormData(data);

    try {
      await fetch("/api/createComment", {
        method: "POST",
        body: JSON.stringify(data)
      });
      setIsSubmitting(false);
      setHasSubmitted(true);
    } catch (err) {
      setFormData(err);
    }
  };

  if (isSubmitting) {
    return <h3 style={{ padding: "1rem" }}>Submitting comment…</h3>;
  }

  if (hasSubmitted) {
    return (
      <div style={{padding: '1rem', listStyleType: 'none'}}>
        <Comment initials={getInitials(formData.name)} name={formData.name} dateString={new Date().toISOString()} text={formData.text} unapproved={true}/>
      </div>
    );
  }

  return (
    <>
      <form onSubmit={handleSubmit(onSubmit)} className={styles.form}>
        <h3>I would love some feedback!</h3>
        <input {...register("_id")} type="hidden" name="_id" value={_id} />

        <label>
          <span>Name</span>
          <input name="name" {...register("name")} placeholder="Your name" />
        </label>

        <label>
          <span>Comment</span>
          <textarea {...register("text")} name="text" rows={8} placeholder="Your feedback" />
        </label>

        <button type="submit">Submit</button>
      </form>
    </>
  );
}
