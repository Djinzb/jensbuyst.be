import CoverImage from "../cover-image";
import Link from "next/link";
import styles from "./heroPost.module.scss";
import { PostMeta } from "../post-meta";

export default function HeroPost({
                                   title,
                                   coverImage,
                                   date,
                                   excerpt,
                                   slug,
                                   timeToRead,
                                   viewCount
                                 }) {
  return (
    <article className={styles.heroPost}>
      <div className={styles.heroImage}>
        <CoverImage slug={slug} title={title} image={coverImage} priority />
      </div>
      <div className={styles.intro}>
        <div>
          <h2>
            <Link href={`/brain-dumps/${slug}`} className="hover:underline">
              {title}
            </Link>
          </h2>
          <PostMeta dateString={date} timeToRead={timeToRead} viewCount={viewCount} />
        </div>
        <div>
          <p className={styles.excerpt}>{excerpt}</p>
        </div>
      </div>
    </article>
  );
}
