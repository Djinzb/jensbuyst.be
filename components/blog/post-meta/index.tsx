import styles from "./postMeta.module.scss";
import Date from "../date";
import { RiEye2Line, RiTimerLine } from "react-icons/ri";

export function PostMeta(props: { dateString: string, timeToRead: number, viewCount: number }) {
  return (
    <div className={styles.meta}>
      <Date dateString={props.dateString} />
      <span> <RiTimerLine />{props.timeToRead} min read</span>
      <span> <RiEye2Line />{props.viewCount} views</span>
    </div>
  );
}
