import CoverImage from "./cover-image";
import PostTitle from "./post-title";
import { PostMeta } from "./post-meta";

export default function PostHeader({ title, coverImage, date, viewCount, timeToRead }) {
  return (
    <div className={"px-4"}>
      <div className="mb-2 sm:mx-0">
        <CoverImage title={title} image={coverImage} priority />
      </div>
      <PostTitle>{title}</PostTitle>
      <div className="ml-4 mb-8 md:mb-16">
        <PostMeta dateString={date} timeToRead={timeToRead} viewCount={viewCount} />
      </div>
      <div className="max-w-2xl mx-auto">
      </div>
    </div>
  );
}
