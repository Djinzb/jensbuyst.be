import CoverImage from "../cover-image";
import Link from "next/link";
import styles from "./morePosts.module.scss";
import { PostMeta } from "../post-meta";

export default function Index({
                                title,
                                coverImage,
                                date,
                                excerpt,
                                slug,
                                timeToRead,
                                viewCount
                              }) {
  return (
    <article className={styles.morePosts}>
      <div className={styles.image}>
        <CoverImage slug={slug} title={title} image={coverImage} priority={false} />
      </div>
      <div className={styles.intro}>
        <h3>
          <Link href={`/brain-dumps/${slug}`} className="hover:underline">
            {title}
          </Link>
        </h3>
        <PostMeta dateString={date} timeToRead={timeToRead} viewCount={viewCount} />
      </div>
      <p className={styles.excerpt}>{excerpt}</p>

    </article>
  );
}
