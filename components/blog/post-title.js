export default function PostTitle({ children }) {
  return (
    <h1 className="text-6xl md:text-6xl lg:text-7xl font-light tracking-tighter leading-tight md:leading-none mb-2 md:text-left">
      {children}
    </h1>
  );
}
