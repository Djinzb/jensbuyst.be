import PostPlug from "../blog/more-posts";
import { Divider } from "./divider/divider";
import styles from "../blog/more-posts/morePosts.module.scss";

export default function MoreStories({ posts }) {
  return (
    <section className={styles.morePosts}>
      <Divider />
      <h2 className="mb-8 text-5xl md:text-7xl font-bold tracking-tighter leading-tight">
        More braindumps
      </h2>
      <div className="grid grid-cols-1 md:grid-cols-2 md:gap-x-16 lg:gap-x-32 gap-y-20 md:gap-y-32 mb-32">
        {posts.map((post) => (
          <PostPlug
            key={post.slug}
            title={post.title}
            coverImage={post.coverImage}
            date={post.date}
            author={post.author}
            slug={post.slug}
            excerpt={post.excerpt}
            timeToRead={post.estimatedReadingTime}
            viewCount={post.viewCount}
          />
        ))}
      </div>
    </section>
  );
}
