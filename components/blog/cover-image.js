import cn from "classnames";
import Image from "next/image";
import Link from "next/link";
import { urlForImage } from "../../lib/sanity";

export default function CoverImage({ title, slug, image: source, priority }) {
  const image = source?.asset?._ref ? (
    <div
      className={cn("shadow-small", {
        "hover:shadow-medium transition-shadow duration-200 br-1": slug,
      })}
    >
      <Image
        className="w-full rounded"
        width={1600}
        height={800}
        alt={`Cover Image for ${title}`}
        src={urlForImage(source).height(800).width(1600).url()}
        sizes="100vw"
        priority={priority}
      />
    </div>
  ) : (
    <div style={{ paddingTop: "50%", backgroundColor: "#ddd" }} />
  );

  return (
    <div className="sm:mx-0">
      {slug ? (
        <Link href={`/brain-dumps/${slug}`} aria-label={title}>
          {image}
        </Link>
      ) : (
        image
      )}
    </div>
  );
}
