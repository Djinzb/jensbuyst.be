import styles from "./nav.module.scss";
import Link from "next/link";
import { useRouter } from "next/router";

const buildRoutes = (path) => {
  const routes = {
    "/": [
      // {
      //   path: "/about",
      //   text: "About",
      // },
      {
        path: "/brain-dumps",
        text: "Brain dumps"
      }
    ],
    "/about": [
      {
        path: "/",
        text: "Home"
      },
      {
        path: "/brain-dumps",
        text: "Brain dumps"
      }
    ],
    "/brain-dumps": [
      {
        path: "/",
        text: "Home"
      }
      // {
      //   path: "/about",
      //   text: "About",
      // },
    ],
    "/brain-dumps/[slug]": [
      {
        path: "/brain-dumps",
        text: "All posts"
      }
    ]
  };
  return routes[path];
};

export const Navigation = ({ title }) => {
  const router = useRouter();
  const routes = buildRoutes(router.pathname);

  return (
    <nav className={styles.nav}>
      <h1 className={styles.h1}>{title}</h1>
      <ul className={styles.navItems}>
        {routes.map((route, i) => (
          <li key={`nav${i}`}>
            <Link href={route.path}>
              {`<${route.text} />`}
            </Link>
          </li>
        ))}
      </ul>
    </nav>
  );
};
