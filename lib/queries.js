const postFields = `
  _id,
  name,
  title,
  date,
  excerpt,
  coverImage,
  "slug": slug.current,
  "author": author->{name, picture},
  "estimatedReadingTime": round(length(pt::text(content)) / 5 / 180 ),
  viewCount
`;

export const indexQuery = `
*[_type == "post"] | order(date desc, _updatedAt desc) {
  ${postFields}
}`;

export const postQuery = `
{
  "post": *[_type == "post" && slug.current == $slug] | order(_updatedAt desc) [0] {
    content,
    ${postFields},
    'comments': *[_type == "comment" && post._ref == ^._id && approved == true]{
            _id, 
            name,
            text, 
            _createdAt
    }
  },
  "morePosts": *[_type == "post" && slug.current != $slug] | order(date desc, _updatedAt desc) [0...2] {
    content,
    ${postFields},
  }
}`;

export const postSlugsQuery = `
*[_type == "post" && defined(slug.current)][].slug.current
`;

export const postBySlugQuery = `
*[_type == "post" && slug.current == $slug][0] {
  ${postFields},
  viewCount: 'comments': *[_type == "viewCount" && post._ref == ^._id]{
            _id, 
            count
            }
}
`;

export const homePageQuery = `
*[_type == "home"] {
  _id, intro
}`;
