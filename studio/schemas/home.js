export const home = {
  name: "home",
  title: "Home page",
  type: "document",
  fields: [
    {
      name: "intro",
      title: "Intro",
      type: "string",
    },
  ],
};
